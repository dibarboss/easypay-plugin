<?php
global $MESS;

$MESS['EP_MODULE_NAME'] = 'Платежная система EasyPay';
$MESS['EP_MODULE_DESC'] = 'Обработчик для платежной системы EasyPay';

$MESS['SECRET_KEY'] = 'Секретный ключ для формирования подписи';
$MESS['SECRET_KEY_DESC'] = '(известен только мерчанту и EasyPay)';
$MESS['MERCHANT_ID'] = 'Идентификатор сервиса торговца';
$MESS['MERCHANT_ID_DESC'] = 'merchant_id';
$MESS['ORDER_ID'] = 'Уникальный ID покупки в Вашем магазине';
$MESS['AMOUNT'] = 'Сумма для списания при оплате в магазине';

$MESS['URL_SUCCESS'] = 'Страница успеха';
$MESS['URL_FAILER'] = 'Страница ошибки';
$MESS['URL_NOTIFY'] = 'Страница оповещения магазина';

$MESS['EP_ACTION'] = 'EasyPay API URL';
$MESS['ACTION_DESC'] = '(атрибут action формы для приема платежей)';