<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
include(GetLangFileName(dirname(__FILE__) . '/', '/payment.php'));

$order_id = (strlen(CSalePaySystemAction::GetParamValue('ORDER_ID')) > 0)
    ? CSalePaySystemAction::GetParamValue('ORDER_ID')
    : $GLOBALS['SALE_INPUT_PARAMS']['ORDER']['ID'];

$amount = (strlen(CSalePaySystemAction::GetParamValue('AMOUNT')) > 0)
    ? CSalePaySystemAction::GetParamValue('AMOUNT')
    : $GLOBALS['SALE_INPUT_PARAMS']['ORDER']['SHOULD_PAY'];


$secret_key = CSalePaySystemAction::GetParamValue('SECRET_KEY');
$merchant_id = CSalePaySystemAction::GetParamValue('MERCHANT_ID');
$desc = '';
$url_success = CSalePaySystemAction::GetParamValue('URL_SUCCESS');
$url_failed = CSalePaySystemAction::GetParamValue('URL_FAILER');
$url_notify = CSalePaySystemAction::GetParamValue('URL_NOTIFY');
$expire_date = '';
$recurrent_payment = 'false';
$recurrent_payment_period = '* * 10 * *';
$recurrent_payment_max_amount = '1.00';

$sign = '';
if (isset($secret_key)) {
    $data = ($secret_key . $merchant_id . $order_id . $amount . $desc . $url_success . $url_failed . $url_notify . $expire_date . $recurrent_payment . $recurrent_payment_period . $recurrent_payment_max_amount);
    $sign = base64_encode(hash('SHA256', $data, true));
}

if (!$action = CSalePaySystemAction::GetParamValue('EP_ACTION')) {
    $action = 'https://easypay.ua/merchant/2_3/order';
}
?>

<?= GetMessage('PAYMENT_DESCRIPTION_EP') ?> <b>www.easypay.ua</b>.<br/>
<?= GetMessage('PAYMENT_DESCRIPTION_SUM') ?>: <b><?= CurrencyFormat($amount, 'UAH') ?></b>
<br/><br/>
<form action="<?= $action ?> " method="post">
    <input type="hidden" name="merchant_id" value="<?= $merchant_id ?>"/>
    <input type="hidden" name="order_id" value="<?= $order_id ?>"/>
    <input type="hidden" name="amount" value="<?= $amount ?>"/>
    <input type="hidden" name="desc" value="<?= $desc ?>"/>
    <input type="hidden" name="url_success" value="<?= $url_success ?>"/>
    <input type="hidden" name="url_failed" value="<?= $url_failed ?>"/>
    <input type="hidden" name="url_notify" value="<?= $url_notify ?>"/>
    <input type="hidden" name="template" value="whitepage"/>
    <input type="hidden" name="expire_date" value=""/>
    <input type="hidden" name="recurrent_payment" value="<?= $recurrent_payment ?>"/>
    <input type="hidden" name="recurrent_payment_period" value="<?= $recurrent_payment_period ?>"/>
    <input type="hidden" name="recurrent_payment_max_amount" value="<?= $recurrent_payment_max_amount ?>"/>
    <input type="hidden" name="sign" value="<?= $sign ?>"/>
    <input type="submit" value="Перейти к оплате" class="btn btn-default">
</form>