<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

if (!isset($_POST['sign'])) {
    die();
}

$data = $_POST;
$received_sign = $_POST['sign'];

$action = $data['action'];
$received_merchant_id = $data['merchant_id'];
$order_id = $data['order_id'];
$amount = $data['amount'];
$desc = $data['desc'];
$payment_id = $data['payment_id'];
$date = $data['date'];
$recurrent_id = $data['recurrent_id'];

if ($order_id <= 0) {
    die();
}
if (!($arOrder = CSaleOrder::GetByID($order_id))) {
    die();
}
if ($arOrder['PAYED'] == 'Y') {
    die();
}

CSalePaySystemAction::InitParamArrays($arOrder, $arOrder['ID']);

$secret_key = CSalePaySystemAction::GetParamValue('SECRET_KEY');
$merchant_id = CSalePaySystemAction::GetParamValue('MERCHANT_ID');

$generated_sign = base64_encode(sha256($secret_key . $action . $received_merchant_id . $order_id . $amount . $desc . $payment_id . $date . $recurrent_id, 1));

if ($received_sign != $generated_sign || $merchant_id != $received_merchant_id) {
    die();
}

if ($action == 'payment') {
    //here you can update your order
    $sDescription = '';
    $sStatusMessage = '';

    $sDescription .= 'order_id: ' . $order_id . '; ';
    $sDescription .= 'amount: ' . $amount . '; ';

    $sStatusMessage .= 'status: ' . $action . '; ';
    $sStatusMessage .= 'date: ' . $date . '; ';
    $sStatusMessage .= 'payment_id: ' . $payment_id . '; ';
    $sStatusMessage .= 'recurrent_id: ' . $recurrent_id . '; ';

    $arFields = array(
        'PS_STATUS' => 'Y',
        'PS_STATUS_CODE' => $action,
        'PS_STATUS_DESCRIPTION' => $sDescription,
        'PS_STATUS_MESSAGE' => $sStatusMessage,
        'PS_SUM' => $amount,
        'PS_CURRENCY' => 'UAH',
        'PS_RESPONSE_DATE' => date(CDatabase::DateFormatToPHP(CLang::GetDateFormat('FULL', LANG))),
    );

    CSaleOrder::PayOrder($arOrder['ID'], 'Y');
    CSaleOrder::Update($arOrder['ID'], $arFields);
}