<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) { die(); }

include(GetLangFileName(dirname(__FILE__).'/', '/.description.php'));

$psTitle = GetMessage('EP_MODULE_NAME');
$psDescription = GetMessage('EP_MODULE_DESC');

$arPSCorrespondence = array(
	'SECRET_KEY' => array(
		'NAME'  => GetMessage('SECRET_KEY'),
		'DESCR' => GetMessage('SECRET_KEY_DESC'),
		'VALUE' => '',
		'TYPE'  => ''
	),
    'MERCHANT_ID' => array(
		'NAME'  => GetMessage('MERCHANT_ID'),
		'DESCR' => GetMessage('MERCHANT_ID_DESC'),
		'VALUE' => '',
		'TYPE'  => ''
	),
	'URL_NOTIFY' => array(
		'NAME'  => GetMessage('URL_NOTIFY'),
		'DESCR' => "",
		'VALUE' => 'https://'.$_SERVER['HTTP_HOST'].'/personal/ps_result.php',
		'TYPE'  => ''
	),
    'URL_FAILER' => array(
		'NAME'  => GetMessage('URL_FAILER'),
		'DESCR' => "",
		'VALUE' => '',
		'TYPE'  => ''
	),
    'URL_SUCCESS' => array(
		'NAME'  => GetMessage('URL_SUCCESS'),
		'DESCR' => "",
		'VALUE' => 'https://'.$_SERVER['HTTP_HOST'].'/personal/order/',
		'TYPE'  => ''
	),
	'AMOUNT' => array(
		'NAME'  => GetMessage('AMOUNT'),
		'DESCR' => '',
		'VALUE' => 'SHOULD_PAY',
		'TYPE'  => 'ORDER'
	),
	'ORDER_ID' => array(
		'NAME'  => GetMessage('ORDER_ID'),
		'DESCR' => '',
		'VALUE' => 'ID',
		'TYPE'  => 'ORDER'
	),
	'EP_ACTION' => array(
		'NAME'  => GetMessage('EP_ACTION'),
		'DESCR' => GetMessage('ACTION_DESC'),
		'VALUE' => 'https://easypay.ua/merchant/2_3/order',
		'TYPE'  => ''
	),

);