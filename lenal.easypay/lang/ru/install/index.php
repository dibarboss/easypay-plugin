<?php
/**
 * EasyPay Payment Module
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category        easypay
 * @package         easy.pay
 * @version         2.3
 * @author          easypay.ua
 * @copyright       Copyright (c) 2018 easypay
 * @license         http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 *
 * EXTENSION INFORMATION
 *
 * 1C-Bitrix        18.0
 * API       https://easypay.ua/merchant-resources/merchant-2_3.html
 *
 */

$MESS['EP_MODULE_NAME'] = 'Платежная система easypay.ua';
$MESS['EP_MODULE_DESC'] = 'Обработчик для платежной системы easypay.ua';
$MESS['EP_ERR_MODULE_NOT_FOUND'] = 'Для установки данного решения необходимо наличие модуля #MODULE#.';